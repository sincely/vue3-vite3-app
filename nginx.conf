# 全局段配置
# 指定运行nginx的用户或用户组，默认为nobody。
# https://juejin.cn/post/7141745637329993736
#user administrator administrators;

# 设置工作进程数，通常设置为等于CPU核心数。
worker_processes 2;

# 指定nginx进程的PID文件存放位置。
# pid /nginx/pid/nginx.pid;
# 指定错误日志的存放路径和日志级别。
error_log log/error.log debug;

events {
  # 设置网络连接序列化，用于防止多个进程同时接受到新连接的情况，这种情况称为"惊群"。
  accept_mutex on;

  # 设置一个进程是否可以同时接受多个新连接。
  multi_accept on;

  # 设置工作进程的最大连接数。
  worker_connections 1024;
}


http {
  # 包含文件扩展名与MIME类型的映射。
  include mime.types;

  # 设置默认的MIME类型。
  default_type application/octet-stream;

  # 定义日志格式。
  log_format access '$remote_addr - $remote_user [$time_local] $host "$request" '
  '$status $body_bytes_sent "$http_referer" '
  '"$http_user_agent" "$http_x_forwarded_for" "$clientip"';

  # 指定访问日志的存放路径和使用的格式。
  access_log log/access.log myFormat;

  # 允许使用sendfile方式传输文件。
  sendfile on;


  # 负载均衡，详细可看了一篇文章：https://learnku.com/articles/36737
  upstream 192.168.0.103:8080 {
    # upstream的负载均衡，weight是权重，可以根据机器配置定义权重。weigth参数表示权值，权值越高被分配到的几率越大
    server 192.168.80.121:80 weight=3;
    server 192.168.80.122:80 weight=2;
    server 192.168.80.123:80 weight=3;
  }


  #设定请求缓冲
  client_header_buffer_size 128k;
  large_client_header_buffers 4 128k;

  #上传文件的大小限制  默认1m
  client_max_body_size 8m;

  # 开启gzip
  gzip on;
  # 最小压缩文件大小
  gzip_min_length 1k;
  # 压缩缓冲区
  gzip_buffers 4 16k;
  # 压缩版本（默认1.1，前端如果是squid2.5请使用1.0）
  gzip_http_version 1.0;
  # 压缩等级
  gzip_comp_level 2;
  # 压缩类型，默认就已经包含text/html，所以下面就不用再写了，写上去也不会有问题，但是会有一个warn。
  gzip_types text/plain application/x-javascript text/css application/xml;
  gzip_vary on;


  # 限制每次调用sendfile传输的数据量。
  sendfile_max_chunk 100k;

  # 设置连接的保持时间,自动断开
  keepalive_timeout 65;

  # 直接请求nginx也是会报跨域错误的这里设置允许跨域
  # 如果代理地址已经允许跨域则不需要这些, 否则报错(虽然这样nginx跨域就没意义了)
  # add_header Access-Control-Allow-Origin *;
  # add_header Access-Control-Allow-Headers X-Requested-With;
  # add_header Access-Control-Allow-Methods GET,POST,OPTIONS;

  # 第一个服务
  server {
    # 设置监听的端口和地址。
    listen 8080;

    # 设置监听的地址,浏览器访问域名, 域名可以有多个，用空格隔开
    server_name localhost;


    # 定义location块，用于匹配特定的请求URI。
    location / {
      # 设置请求的根目录。
      root html;

      # 设置默认入口文件页面。
      index index.html index.htm;
    }

    # 设置代理  localhost /PMS 的请求会被转发到192.168.0.103:8080
    # location ~ /PMS/ {
    #   rewrite ^/b/(.*)$ /$1 break; # 去除本地接口/api前缀, 否则会出现404
    #   proxy_set_header Host $host;
    #   proxy_set_header X-Real-IP $remote_addr;
    #   proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    #   proxy_pass http://192.168.0.103:8080;
    # }

    # 禁止页面缓存
    location ~* \.(js|css|png|jpg|gif)$ {
      add_header Cache-Control no-store;
    }

    # 添加请求头
    add_header Access-Control-Allow-Origin *;

    # 根据状态码，返回对于的错误页面
    error_page 500 502 503 504 /50x.html;

    location = /50x.html {
      root html;
    }
  }

  # 引入其他的配置文件
  # include servers/*;
}
