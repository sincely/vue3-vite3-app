import './style.css'
import App from './App.vue'
import router from './router' // 路由
import './styles/index.less' // 全局样式
createApp(App).use(router).mount('#app')
